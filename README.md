## Prueba Tecnica EY Jordi
En esta prueba técnica se ha creado una barra de navegación.
Realizando con la metodología  BEM, con una estructura y organización diferentes a la de Alejandro, utilizando el framework en la parte mobile de [Tailwindcss](https://tailwindcss.com/).
Aún así, hemos trabajado conjuntamente en algunos puntos haciendo cada uno su propia nav
bar.
Hemos querido hacer dos proyectos diferentes para que así veais dos maneras de trabajar
como también nuestra capacidad de adaptarnos e improvisar según lo demandado.
## Estructura del proyecto:
src<br />
 - Assets -> Imágenes del proyecto.<br />
 - Components :
        - DesktopComponent -> Componente creado con BEM. <br />
        - MobileComponent -> Componente creado con base tawling.<br />
 - Styles | Sass | Abstracto 
                - Mixins -> Contiene los querys responsive<br />
                             - _variables -> Contiene variables globales como primer color, segundo color, breakpoints responsive.<br />
                 -Components -> Carpeta donde irán todos los scss.<br />
                                - DesktopComponent.scss -> Estilos del componente desktop.<br />
 - App.scss -> Scss general de la aplicación.<br />
 - App.vue -> Contiene métodos para renderizar el componente Mobile o Desktop dependiendo del rize que tenga la pantalla.<br />
 
 
## Ejecutar el proyecto

He usado la versión de node v16.15.1  ya que es la ultima versión LTS , se ha includio en el fichero .nvmrc .

### Primer paso instalar dependencias 

```
yarn install
```

### Segundo paso ejecutar el proyecto

```
yarn serve
```

### Tercer visualizar el proyecto en el navegador:

```
  Local:   http://localhost:8080/ 
```


### Customize configuration
See [Yarn Reference](https://yarnpkg.com/).
See [Configuration Reference](https://cli.vuejs.org/config/).
