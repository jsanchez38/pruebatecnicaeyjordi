/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        'regal-blue': '#0A2640'
      },
      animation: {
        // fade: 'fadeOut  forwards',
        // bounceShort: 'fadeOut 1s ease-in-out 2',
        fade: 'fadeOut 10s ease-in-out'
      },
      keyframes: theme => ({
        // fadeOut: {
        //   '100%': { backgroundColor: theme('colors.red.300') },
        //   '50%': { backgroundColor: theme('colors.red.100') },

        //   '0%': { backgroundColor: theme('colors.transparent') }
        // }
        fadeOut: {
          '0%': { opacity: theme('0') },
          '100%': { opacity: theme('1') }
        }
      })
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
